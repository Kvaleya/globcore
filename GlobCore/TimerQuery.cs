﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Graphics.OpenGL;

namespace GlobCore
{
	class TimerQueryManager
	{
		List<TimerQuery> _queries = new List<TimerQuery>();

		public IReadOnlyList<TimerQuery> FinishQueries()
		{
			foreach (TimerQuery query in _queries)
			{
				query.MeasureAndDisposeQueries();
			}
			return _queries;
		}

		public TimerQuery CreateQuery(string name)
		{
			var q = new TimerQuery(name);
			_queries.Add(q);
			return q;
		}
	}

	public class TimerQuery
	{
		public static bool Allow = true;

		internal double Start { get; private set; }
		internal double End { get; private set; }
		public string Name { get; private set; }

		internal bool ValidMeasurement { get { return _validMeasurement; } }

		public double Elapsed { get { return End - Start; } }

		int _query0;
		int _query1;
		bool _ended = false;
		bool _validMeasurement = false;

		internal TimerQuery(string name)
		{
			Name = name;
		}

		public void StartQuery()
		{
			if (!Allow)
				return;

			_query0 = GL.GenQuery();
			_query1 = GL.GenQuery();
			GL.QueryCounter(_query0, QueryCounterTarget.Timestamp);
		}

		public void EndQuery()
		{
			_ended = true;
			if (_query0 == 0 || _query1 == 0)
				return;
			GL.QueryCounter(_query1, QueryCounterTarget.Timestamp);
		}

		internal void MeasureAndDisposeQueries()
		{
			if (_query0 == 0 || _query1 == 0)
				return;

			if (!_ended)
			{
				throw new Exception("GPU Timer Query was started but never ended!");
			}

			// wait until the query results are available
			int i = 0;
			while (i == 0)
			{
				GL.GetQueryObject(_query1, GetQueryObjectParam.QueryResultAvailable, out i);
			}

			long start, end;

			// get the query results
			GL.GetQueryObject(_query0, GetQueryObjectParam.QueryResult, out start);
			GL.GetQueryObject(_query1, GetQueryObjectParam.QueryResult, out end);
			Start = start * 1E-9;
			End = end * 1E-9;

			GL.DeleteQuery(_query0);
			GL.DeleteQuery(_query1);

			_query0 = 0;
			_query1 = 0;

			_validMeasurement = true;
		}

		public override string ToString()
		{
			return Name + ": " + Math.Round(Elapsed * 1000, 2).ToString() + " ms";
		}
	}
}
