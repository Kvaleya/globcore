﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace GlobCore
{
	public class GlobalUniforms
	{
		Dictionary<string, IUniformValue> _uniforms = new Dictionary<string, IUniformValue>();
		Dictionary<string, Texture> _textures = new Dictionary<string, Texture>();

		internal GlobalUniforms()
		{
		}

		void SetUniform(string name, IUniformValue uniformValue)
		{
			if (_uniforms.ContainsKey(name))
			{
				_uniforms[name] = uniformValue;
			}
			else
			{
				_uniforms.Add(name, uniformValue);
			}
		}

		public void SetTexture(string name, Texture texture)
		{
			if (_textures.ContainsKey(name))
			{
				_textures[name] = texture;
			}
			else
			{
				_textures.Add(name, texture);
			}
		}

		internal void BindTo(Device device, Shader shader)
		{
			foreach(var pair in _uniforms)
			{
				shader.SetUniform(pair.Key, pair.Value);
			}

			foreach((string name, Texture tex) in _textures)
			{
				if (!tex.Valid)
					continue;

				int binding = shader.GetTextureBinding(name);
				
				if (binding >= 0)
				{
					device.BindTexture(tex, binding);
				}
			}
		}

		#region Uniform setting

		public void SetUniformF(string uniform, float value)
		{
			IUniformValue uniformValue = new UniformFloat(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformF(string uniform, Vector2 value)
		{
			IUniformValue uniformValue = new UniformVector2(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformF(string uniform, Vector3 value)
		{
			IUniformValue uniformValue = new UniformVector3(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformF(string uniform, Vector4 value)
		{
			IUniformValue uniformValue = new UniformVector4(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformI(string uniform, int x)
		{
			IUniformValue uniformValue = new UniformInt(x);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformI(string uniform, int x, int y)
		{
			IUniformValue uniformValue = new UniformVector2Int(x, y);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformI(string uniform, int x, int y, int z)
		{
			IUniformValue uniformValue = new UniformVector3Int(x, y, z);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformI(string uniform, int x, int y, int z, int w)
		{
			IUniformValue uniformValue = new UniformVector4Int(x, y, z, w);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformI(string uniform, Vector2i v)
		{
			IUniformValue uniformValue = new UniformVector2Int(v.X, v.Y);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformI(string uniform, Vector3i v)
		{
			IUniformValue uniformValue = new UniformVector3Int(v.X, v.Y, v.Z);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformI(string uniform, Vector4i v)
		{
			IUniformValue uniformValue = new UniformVector4Int(v.X, v.Y, v.Z, v.W);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformD(string uniform, double value)
		{
			IUniformValue uniformValue = new UniformDouble(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformD(string uniform, Vector2d value)
		{
			IUniformValue uniformValue = new UniformVector2Double(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformD(string uniform, Vector3d value)
		{
			IUniformValue uniformValue = new UniformVector3Double(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformD(string uniform, Vector4d value)
		{
			IUniformValue uniformValue = new UniformVector4Double(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformF(string uniform, Matrix2 value)
		{
			IUniformValue uniformValue = new UniformMatrix2(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformF(string uniform, Matrix3 value)
		{
			IUniformValue uniformValue = new UniformMatrix3(value);
			SetUniform(uniform, uniformValue);
		}

		public void SetUniformF(string uniform, Matrix4 value)
		{
			IUniformValue uniformValue = new UniformMatrix4(value);
			SetUniform(uniform, uniformValue);
		}

		#endregion
	}
}
