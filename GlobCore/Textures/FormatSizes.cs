﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GlobCore
{
	static class FormatSizes
	{
		public static long GetSize(long pixels, SizedInternalFormatGlob format)
		{
			return pixels * _sizes[format];
		}

		static readonly IReadOnlyDictionary<SizedInternalFormatGlob, int> _sizes = new Dictionary<SizedInternalFormatGlob, int>()
		{
			{ SizedInternalFormatGlob.R8, 1 },
			{ SizedInternalFormatGlob.R16, 2 },
			{ SizedInternalFormatGlob.RG8, 2 },
			{ SizedInternalFormatGlob.RG16, 4 },
			{ SizedInternalFormatGlob.R16F, 2 },
			{ SizedInternalFormatGlob.R32F, 4 },
			{ SizedInternalFormatGlob.RG16F, 4 },
			{ SizedInternalFormatGlob.RG32F, 8 },
			{ SizedInternalFormatGlob.R8I, 1 },
			{ SizedInternalFormatGlob.R8UI, 1 },
			{ SizedInternalFormatGlob.R16I, 2 },
			{ SizedInternalFormatGlob.R16UI, 2 },
			{ SizedInternalFormatGlob.R32I, 4 },
			{ SizedInternalFormatGlob.R32UI, 4 },
			{ SizedInternalFormatGlob.RG8I, 2 },
			{ SizedInternalFormatGlob.RG8UI, 2 },
			{ SizedInternalFormatGlob.RG16I, 4 },
			{ SizedInternalFormatGlob.RG16UI, 4 },
			{ SizedInternalFormatGlob.RG32I, 8 },
			{ SizedInternalFormatGlob.RG32UI, 8 },

			{ SizedInternalFormatGlob.R3_G3_B2, 1 },
			{ SizedInternalFormatGlob.RGB4, 0 },
			{ SizedInternalFormatGlob.RGB5, 0 },
			{ SizedInternalFormatGlob.RGB8, 0 },
			{ SizedInternalFormatGlob.RGB10, 0 },
			{ SizedInternalFormatGlob.RGB12, 0 },
			{ SizedInternalFormatGlob.RGB16, 0 },
			{ SizedInternalFormatGlob.RGBA2, 0 },
			{ SizedInternalFormatGlob.RGBA4, 0 },
			{ SizedInternalFormatGlob.RGB5_A1, 0 },
			{ SizedInternalFormatGlob.RGBA8, 0 },
			{ SizedInternalFormatGlob.RGB10_A2, 4 },
			{ SizedInternalFormatGlob.RGBA12, 0 },
			{ SizedInternalFormatGlob.RGBA16, 0 },

			{ SizedInternalFormatGlob.R8_SNORM, 1 },
			{ SizedInternalFormatGlob.RG8_SNORM, 2 },
			{ SizedInternalFormatGlob.RGB8_SNORM, 4 },
			{ SizedInternalFormatGlob.RGBA8_SNORM, 4 },
			{ SizedInternalFormatGlob.R16_SNORM, 2 },
			{ SizedInternalFormatGlob.RG16_SNORM, 4 },
			{ SizedInternalFormatGlob.RGB16_SNORM, 8 },
			{ SizedInternalFormatGlob.RGBA16_SNORM, 8 },

			{ SizedInternalFormatGlob.RGB10_A2UI, 4 },

			{ SizedInternalFormatGlob.SRGB8, 4 },
			{ SizedInternalFormatGlob.SRGB8_ALPHA8, 4 },

			{ SizedInternalFormatGlob.RGBA32F, 16 },
			{ SizedInternalFormatGlob.RGB32F, 16 },
			{ SizedInternalFormatGlob.RGBA16F, 8 },
			{ SizedInternalFormatGlob.RGB16F, 8 },

			{ SizedInternalFormatGlob.R11F_G11F_B10F, 4 },
			{ SizedInternalFormatGlob.RGB9_E5, 4 },

			{ SizedInternalFormatGlob.RGBA32UI, 16 },
			{ SizedInternalFormatGlob.RGB32UI, 16 },
			{ SizedInternalFormatGlob.RGBA16UI, 8 },
			{ SizedInternalFormatGlob.RGB16UI, 8 },
			{ SizedInternalFormatGlob.RGBA8UI, 4 },
			{ SizedInternalFormatGlob.RGB8UI, 4 },
			{ SizedInternalFormatGlob.RGBA32I, 16 },
			{ SizedInternalFormatGlob.RGB32I, 16 },
			{ SizedInternalFormatGlob.RGBA16I, 8 },
			{ SizedInternalFormatGlob.RGB16I, 8 },
			{ SizedInternalFormatGlob.RGBA8I, 4 },
			{ SizedInternalFormatGlob.RGB8I, 4 },

			{ SizedInternalFormatGlob.DEPTH_COMPONENT16, 2 },
			{ SizedInternalFormatGlob.DEPTH_COMPONENT24, 3 },
			{ SizedInternalFormatGlob.DEPTH_COMPONENT32, 4 },

			{ SizedInternalFormatGlob.DEPTH_COMPONENT32F, 4 },
			{ SizedInternalFormatGlob.DEPTH32F_STENCIL8, 5 },
			{ SizedInternalFormatGlob.DEPTH24_STENCIL8, 4 },

			{ SizedInternalFormatGlob.STENCIL_INDEX8, 1 },

			{ SizedInternalFormatGlob.COMPRESSED_RED_RGTC1, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SIGNED_RED_RGTC1, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RG_RGTC2, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SIGNED_RG_RGTC2, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RED, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RG, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGB, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGBA, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SRGB, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SRGB_ALPHA, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGBA_BPTC_UNORM, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SRGB_ALPHA_BPTC_UNORM, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGB_BPTC_SIGNED_FLOAT, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT, 0 },

			{ SizedInternalFormatGlob.COMPRESSED_RGB_S3TC_DXT1_EXT, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGBA_S3TC_DXT1_EXT, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGBA_S3TC_DXT3_EXT, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_RGBA_S3TC_DXT5_EXT, 0 },

			{ SizedInternalFormatGlob.COMPRESSED_SRGB_S3TC_DXT1_EXT, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT, 0 },
			{ SizedInternalFormatGlob.COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT, 0 },
		};
	}
}
