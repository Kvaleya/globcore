﻿using System;
using OpenTK.Graphics.OpenGL;

namespace GlobCore
{
	public class Texture3D : Texture
	{
		public int Width { get { return base.StorageWidth; } }
		public int Height { get { return base.StorageHeight; } }
		public int Depth { get { return base.StorageDepth; } }

		public Texture3D(Device device, string name, SizedInternalFormatGlob format, int width, int height, int depth, int levels = 0)
			: base(TextureTarget.Texture3D, name)
		{
			device.BindTexture(Target, Handle);
			TexStorage3D(format, width, height, depth, levels);
		}

		public Texture3D(Device device, string name, SizedInternalFormatGlob format, Texture origTexture, int minLevel, int numLevels, int minLayer = 0, int numLayers = 1)
			: base(TextureTarget.Texture3D, name)
		{
			//state.TextureUnit.BindTexture(Target, Handle);
			TextureView(origTexture, format, minLevel, numLevels, minLayer, numLayers);
		}

		public void TexSubImage3D<T>(Device device, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, T[] pixels)
			where T : struct
		{
			device.BindTexture(Target, Handle);
			GL.TexSubImage3D(Target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
		}

		public void TexSubImage3D(Device device, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, IntPtr pixels)
		{
			device.BindTexture(Target, Handle);
			GL.TexSubImage3D(Target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
		}
	}
}
