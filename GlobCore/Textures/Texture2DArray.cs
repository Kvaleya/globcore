﻿using System;
using OpenTK.Graphics.OpenGL;

namespace GlobCore
{
	public class Texture2DArray : Texture
	{
		public int Width { get { return base.StorageWidth; } }
		public int Height { get { return base.StorageHeight; } }
		public int Layers { get { return base.StorageDepth; } }

		public Texture2DArray(Device device, string name, SizedInternalFormatGlob format, int width, int height, int layers, int levels = 0)
			: base(TextureTarget.Texture2DArray, name)
		{
			device.BindTexture(Target, Handle);
			TexStorage3D(format, width, height, layers, levels);
		}

		public Texture2DArray(Device device, string name, SizedInternalFormatGlob format, Texture origTexture, int minLevel, int numLevels, int minLayer, int numLayers)
			: base(TextureTarget.Texture2DArray, name)
		{
			//state.TextureUnit.BindTexture(Target, Handle);
			TextureView(origTexture, format, minLevel, numLevels, minLayer, numLayers);
		}

		public void TexSubImage2D<T>(Device device, int level, int xoffset, int yoffset, int zoffset, int width, int height, PixelFormat format, PixelType type, T[] pixels)
			where T : struct
		{
			device.BindTexture(Target, Handle);
			GL.TexSubImage3D(Target, level, xoffset, yoffset, zoffset, width, height, 1, format, type, pixels);
		}

		public void TexSubImage2D(Device device, int level, int xoffset, int yoffset, int zoffset, int width, int height, PixelFormat format, PixelType type, IntPtr pixels)
		{
			device.BindTexture(Target, Handle);
			GL.TexSubImage3D(Target, level, xoffset, yoffset, zoffset, width, height, 1, format, type, pixels);
		}

		public void CompressedTexSubImage2D<T>(Device device, int level, int xoffset, int yoffset, int zoffset, int width, int height, T[] pixels)
			where T : struct
		{
			device.BindTexture(Target, Handle);
			GL.CompressedTexSubImage2D(Target, level, xoffset, yoffset, width, height, (PixelFormat)this.Format, pixels.Length, pixels);
			GL.CompressedTexSubImage3D(Target, level, xoffset, yoffset, zoffset, width, height, 1, (PixelFormat)this.Format, pixels.Length, pixels);
		}

		public void CompressedTexSubImage2D(Device device, int level, int xoffset, int yoffset, int zoffset, int width, int height, int numBytes, IntPtr pixels)
		{
			device.BindTexture(Target, Handle);
			GL.CompressedTexSubImage3D(Target, level, xoffset, yoffset, zoffset, width, height, 1, (PixelFormat)this.Format, numBytes, pixels);
		}
	}
}
